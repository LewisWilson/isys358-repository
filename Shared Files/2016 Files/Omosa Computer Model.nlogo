globals [
  grass ;; keep track of how much grass there is
  initial-grass
  grass-regrowth-speed
  yernt-speed ;; how fast the yernt moves compared relatively to tooru
  drought
  yernt-gain-from-food
  yernt-reproduce
  tooru-reproduce
  tooru-gain-from-food
]
breed [fires fire]    ;; bright red turtles -- the leading edge of the fire
breed [embers ember]  ;; turtles gradually fading from red to near black
;; Sheep and wolves are both breeds of turtle.
breed [yernts yernt]  ;; sheep is its own plural, so we use "a-sheep" as the singular.
breed [toorus tooru]
breed [omosans omosan]
turtles-own [energy]       ;; both wolves and sheep have energy
yernts-own [
  tooruflocks
  nearest-tooru
  omosanflocks
  nearest-omosan
  nearest-hunter
]
toorus-own [
  yerntflocks
  nearest-yernt
]
omosans-own [
  preyflocks
  nearest-prey
]
patches-own [countdown]

to setup
  ;; (for this model to work with NetLogo's new plotting features,
  ;; __clear-all-and-reset-ticks should be replaced with clear-all at
  ;; the beginning of your setup procedure and reset-ticks at the end
  ;; of the procedure.)
  __clear-all-and-reset-ticks
  set yernt-gain-from-food 10
  set yernt-reproduce 10
  set tooru-gain-from-food 50
  set tooru-reproduce 2
  ask patches [ set pcolor green ]
  ;; check GRASS? switch.
  ;; if it is true, then grass grows and the sheep eat it
  ;; if it false, then the sheep don't need to eat
  ifelse drought?
    [set grass-regrowth-speed drought-severity-level * (100 - grass-regrowth-time) / 10]
    [set grass-regrowth-speed (100 - grass-regrowth-time)]
  ask patches [
;      set countdown random grass-regrowth-time ;; initialize grass grow clocks randomly
      set countdown random grass-regrowth-speed ;; initialize grass grow clocks randomly
      ifelse fire-stick-farming?
        [set pcolor one-of [green brown grey]]
        [set pcolor one-of [green brown]]
  ]
  if drought? [
    set drought 10
    ask patches with [pcolor != grey]
    [
      if (random-float 100) < drought [ set pcolor one-of [brown]]
      if (random-float 100) < 100 - drought [ set pcolor one-of [green]]
    ]
  ]
  set-default-shape yernts "sheep"
  create-yernts initial-number-yernts  ;; create the sheep, then initialize their variables
  [
    set color 44
    set size 1.5  ;; easier to see
    set label-color blue - 2
    set energy random (2 * yernt-gain-from-food)
    setxy random-xcor random-ycor
  ]
  set-default-shape toorus "wolf"
  create-toorus initial-number-toorus  ;; create the wolves, then initialize their variables
  [
    set color black
    set size 1.5  ;; easier to see
    set energy random (2 * tooru-gain-from-food)
    setxy random-xcor random-ycor
  ]
  if omosan? [
    set-default-shape omosans "person"
    create-omosans initial-number-omosans
    [
      set color blue
      set size 1.5
      set label-color black
      set energy random (2 * (20 + 10))
      setxy random-xcor random-ycor
    ]
  ]
  if fire-stick-farming?
  [
    set-default-shape fires "square"
    set-default-shape embers "square"
    ask patches with [pxcor = min-pxcor]
      [ ignite ]
    ;;set grass count patches with [pcolor = green]
    ;;fire-stick-farming
    ask yernts [ hide-turtle ]
    ask toorus [ hide-turtle ]
    ask omosans [ hide-turtle ]
  ]
  set initial-grass count patches with [pcolor = green or pcolor = brown]
  set yernt-speed 100
  display-labels
  update-plot
end

to ignite
  sprout-fires 1
    [ set color red ]
  set pcolor grey
end

;; achieve fading color effect for the fire as it burns
to fade-embers
  ask embers
    [ set color color - 0.05  ;; make red darker
      if color < red - 3.5     ;; are we almost at black?
          [set pcolor grey
           die] ]
end

to fire-stick-farming
  let brown-grass count patches with [pcolor = brown]
  ifelse (brown-grass + grass) / initial-grass * 100 > 100 - percent-burned-grass
  [
     ask fires
     [ ask neighbors4 with [pcolor = green or pcolor = brown]
           [ ignite ]
       set breed embers]
       fade-embers
  ]
  [
     ifelse any? embers [fade-embers]
     [ ask embers [ die ]
       ask fires [ die ]]
  ]
end

to go
  if not any? turtles [ stop ]
  if (not any? toorus) and (not any? yernts) [stop]
  ifelse any? fires or any? embers [ fire-stick-farming ]
  [
  ask yernts [
    show-turtle
    if ticks mod (1 / (yernt-speed / 100)) = 0 [
      yernt-move
      set energy energy - 1  ;; deduct energy for sheep
    ]
;    set energy energy - 1  ;; deduct energy for sheep
    eat-grass
    death
    reproduce-sheep
  ]
  ask toorus [
    show-turtle
    catch-sheep
    tooru-move
    set energy energy - 1  ;; wolves lose energy as they move
    death
    reproduce-wolves
  ]
  if omosan? [
    ask omosans [
      show-turtle
      omosan-move
      set energy energy - 1
      catch-food
;      death
;      reproduce-humans
    ]
  ]
  ask patches [ grow-grass ]
  ]
  tick
  update-plot
  display-labels
end

to move  ;; turtle procedure
  rt random 50
  lt random 50
  fd 1
end

;;; yernts move

to yernt-move  ;; turtle procedure
  find-hunterflocks
  ifelse (any? tooruflocks) or (any? omosanflocks)
  [ find-nearest-hunter
    if (not any? toorus-here) and (not any? omosans-here)
    [let x-component [sin (towards myself)] of nearest-hunter
     let y-component [cos (towards myself)] of nearest-hunter
     set heading atan x-component y-component
     fd 1]]
    [move]
end

to find-hunterflocks ;; tooru procedure
  set tooruflocks toorus in-radius 5
  set omosanflocks omosans in-radius 5
end

to find-nearest-hunter
  let nearest-tooru-distance 1000000
  let nearest-omosan-distance 1000000
  set nearest-omosan min-one-of omosanflocks [distance myself]
  if any? tooruflocks
  [ set nearest-tooru min-one-of tooruflocks [distance myself]
    set nearest-tooru-distance distance (nearest-tooru)]
  if any? omosanflocks
  [ set nearest-omosan min-one-of omosanflocks [distance myself]
    set nearest-omosan-distance distance nearest-omosan]
  ifelse nearest-omosan-distance < nearest-tooru-distance
  [set nearest-hunter nearest-omosan]
  [set nearest-hunter nearest-tooru]
end

;;; Toorus move

to tooru-move  ;; turtle procedure
  ifelse any? yernts-on neighbors
  [set nearest-yernt one-of yernts-on neighbors
   set xcor [xcor] of nearest-yernt
   set ycor [ycor] of nearest-yernt]
  [ find-yerntflocks
    ifelse any? yerntflocks
    [ find-nearest-yernt
      if not any? yernts-here
      [let x-component [sin (towards myself + 180)] of nearest-yernt
      let y-component [cos (towards myself + 180)] of nearest-yernt
      set heading atan x-component y-component
      fd 1]]
    [move]]
end

to find-yerntflocks ;; tooru procedure
  set yerntflocks yernts in-radius 15
end

to find-nearest-yernt ;; tooru procedure
  set nearest-yernt min-one-of yerntflocks [distance myself]
end

;;; omosans move

to omosan-move
  ifelse any? yernts-on neighbors
  [set nearest-prey one-of yernts-on neighbors
   set xcor [xcor] of nearest-prey
   set ycor [ycor] of nearest-prey]
  [ find-preyflocks
    ifelse any? preyflocks
    [ find-nearest-prey
      if not any? yernts-here
      [let x-component [sin (towards myself + 180)] of nearest-prey
      let y-component [cos (towards myself + 180)] of nearest-prey
      set heading atan x-component y-component
      fd 1]]
    [move]]
end

to find-preyflocks
  set preyflocks yernts in-radius 10
end

to find-nearest-prey
  set nearest-prey min-one-of preyflocks [distance myself]
end
;;;

to eat-grass  ;; sheep procedure
  ;; sheep eat grass, turn the patch brown
  if pcolor = green [
    set pcolor brown
    set energy energy + yernt-gain-from-food  ;; sheep gain energy by eating
  ]
end

to reproduce-sheep  ;; sheep procedure
  if not any? turtles-on neighbors [
  if random-float 100 < yernt-reproduce [  ;; throw "dice" to see if you will reproduce
    set energy (energy / 2)                ;; divide energy between parent and offspring
    hatch 1 [ rt random-float 360 fd 1 ]   ;; hatch an offspring and move it forward 1 step
  ]
  ]
end

to reproduce-wolves  ;; wolf procedure
  if not any? turtles-on neighbors [
  if random-float 100 < tooru-reproduce [  ;; throw "dice" to see if you will reproduce
    set energy (energy / 2)               ;; divide energy between parent and offspring
    hatch 1 [ rt random-float 360 fd 1 ]  ;; hatch an offspring and move it forward 1 step
  ]
  ]
end

;to reproduce-humans
;  if random-float 100 < omosan-reproduce [
;    set energy (energy / 2)
;    hatch 1 [ rt random-float 360 fd 1 ]
;  ]
;end

to catch-sheep  ;; wolf procedure
  let prey one-of yernts-here                    ;; grab a random sheep
  if prey != nobody                             ;; did we get one?  if so,
    [ ask prey [ die ]                          ;; kill it
      set energy energy + tooru-gain-from-food ] ;; get energy from eating
end

to catch-food
  let prey-sheep one-of yernts-here
  if prey-sheep != nobody [
    ask prey-sheep [ die ]
    set energy energy + 20 ]
end

to death  ;; turtle procedure
  ;; when energy dips below zero, die
  if energy < 0 [ die ]
end

to grow-grass  ;; patch procedure
  ;; countdown on brown patches: if reach 0, grow some grass
  if pcolor = brown [
    ifelse countdown <= 0
      [ set pcolor green
        set countdown grass-regrowth-speed ]
      [ set countdown countdown - 1 ]
  ]
end

to update-plot
  set grass count patches with [pcolor = green]
  set-current-plot "populations"
  set-current-plot-pen "yernts"
  plot count yernts
  set-current-plot-pen "toorus"
  plot count toorus
  set-current-plot-pen "omosans"
  plot count omosans
  set-current-plot-pen "grass / 4"
  plot grass / 4  ;; divide by four to keep it within similar
                    ;; range as wolf and sheep populations
  ;;tried to get rid of "grass / 4" on the plot, but this code does not work
  ;;set-current-plot-pen "grass"
  ;;set grass (grass / 4)  ;; divide by four to keep it within similar
                         ;; range as wolf and sheep populations
  ;;plot grass

end

to display-labels
  ask turtles [ set label "" ]
  if show-energy? [
    ask toorus [ set label round energy ]
    ask yernts [ set label round energy ]
    ask omosans [ set label round energy ]
  ]
end


; Copyright (c) 2016 Pallas Advanced Learning Systems Pty Ltd. All rights reserved.
; The full copyright notice is in the Information tab.


@#$#@#$#@
GRAPHICS-WINDOW
373
10
842
500
25
25
9.0
1
14
1
1
1
0
0
0
1
-25
25
-25
25
1
1
1
ticks
30.0

SLIDER
7
233
179
266
initial-number-yernts
initial-number-yernts
0
250
100
1
1
NIL
HORIZONTAL

SLIDER
188
233
359
266
initial-number-toorus
initial-number-toorus
0
250
50
1
1
NIL
HORIZONTAL

SLIDER
9
63
203
96
grass-regrowth-time
grass-regrowth-time
0
100
15
1
1
NIL
HORIZONTAL

BUTTON
10
10
79
43
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
92
10
159
43
go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
8
386
324
583
Populations
time
pop.
0.0
100.0
0.0
100.0
true
true
"" ""
PENS
"Grass / 4" 1.0 0 -10899396 true "" ""
"Yernts" 1.0 0 -4079321 true "" ""
"Toorus" 1.0 0 -16777216 true "" ""
"Omosans" 1.0 0 -13345367 true "" ""

MONITOR
83
334
154
379
Yernts
count yernts
3
1
11

MONITOR
158
334
240
379
Toorus
count toorus
3
1
11

MONITOR
244
334
320
379
Grass
grass / 4
0
1
11

TEXTBOX
11
213
151
232
Yernt settings
11
0.0
0

TEXTBOX
189
214
302
232
Tooru settings
11
0.0
0

TEXTBOX
13
103
165
121
Fire stick farming settings
11
0.0
0

SWITCH
175
11
311
44
show-energy?
show-energy?
1
1
-1000

SWITCH
8
121
164
154
fire-stick-farming?
fire-stick-farming?
0
1
-1000

SLIDER
168
121
341
154
percent-burned-grass
percent-burned-grass
5
95
18
1
1
NIL
HORIZONTAL

SWITCH
7
289
119
322
omosan?
omosan?
1
1
-1000

SLIDER
122
289
294
322
initial-number-omosans
initial-number-omosans
0
100
10
1
1
NIL
HORIZONTAL

MONITOR
10
334
81
379
Omasans
count omosans
17
1
11

TEXTBOX
7
270
157
288
Omosan settings
11
0.0
1

TEXTBOX
9
45
159
63
Grass setting
11
0.0
1

SWITCH
8
177
121
210
drought?
drought?
1
1
-1000

TEXTBOX
9
159
159
177
Drought settings
11
0.0
1

TEXTBOX
343
116
493
134
NIL
11
0.0
1

SLIDER
128
177
338
210
drought-severity-level
drought-severity-level
1
100
30
1
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

This model presents the ecosystem of Omosa, a virtual learning environment that students use to learn science inquiry. The model enables students to explore what happens to animals and humans under conditions such as fire-stick farming, drought, and hunting.

## HOW IT WORKS

There are two main variations to this model.

In the first variation, yernt wander randomly around the landscape, and must eat grass in order to maintain their energy - when they run out of energy they die, while the tooru look for yernt to prey on. Each step costs the tooru energy, and they must eat yernt in order to replenish their energy - when they run out of energy they die. To allow the population to continue, each tooru or yernt has a fixed probability of reproducing at each time step. Once grass is eaten it will only regrow after a fixed amount of time. This variation produces interesting population dynamics, but is generally stable unless there are some effect of fire-stick farming and drought.

The second variation includes omosan in addition to tooru and yernt. By default, omosan only hunt yernt. This variation is more complex than the first, and is ultimately unstable. In addtion, this variaion can be set up in the way that omosan can hunt both yernt and tooru.

## HOW TO USE IT

1. Set the FIRE-STICK-FARMING? or DROUGHT? switch to TRUE to include fire-stick farming and drought in the model, or to FALSE to only normal condition.
2. Set the OMOSAN? switch to TRUE to include Omosan in the model, or to FALSE to only include Tooru and Yernt. Set the OMOSANS-HUNT-BOTH-YERNT-AND-TOORU? to TRUE to allow omosan to hunt boths animals or FALSE otherwise.
3. Adjust the slider parameters (see below), or use the default settings.
4. Press the SETUP button.
5. Press the GO button to begin the simulation.
6. Look at the monitors to see the current population sizes
7. Look at the POPULATIONS plot to watch the populations fluctuate over time

Parameters:
INITIAL-NUMBER-YERNT: The initial size of yernt population
INITIAL-NUMBER-TOORU: The initial size of tooru population
INITIAL-NUMBER-OMOSAN: The initial size of omosan population
YERNT-GAIN-FROM-FOOD: The amount of energy yernt get for every grass patch eaten
TOORU-GAIN-FROM-FOOD: The amount of energy tooru get for every yernt eaten
YERNT-REPRODUCE: The probability of a yernt reproducing at each time step
TOORU-REPRODUCE: The probability of a tooru reproducing at each time step
GRASS?: Whether or not to include grass in the model
GRASS-REGROWTH-TIME: How long it takes for grass to regrow once it is eaten
FIRE-STICK-FARMING?: Whether or not to include fire-stick farming in the model
PERCENT-BURN-GRASS: The percentage of green grasses expected to be burn
DROUGHT?: Whether or not to include drought in the model
DROUGHT-SEVERITY-LEVEL: How severe the drough is or the amount of dryness
SHOW-ENERGY?: Whether or not to show the energy of each animal as a number

Notes:
- one unit of energy is deducted for every step an animal or Omasan takes

## CREDITS AND REFERENCES


## COPYRIGHT NOTICE

Copyright (c) 2016 Pallas Advanced Learning Systems Pty Ltd. All rights reserved.











@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Rectangle -1 true true 166 225 195 285
Rectangle -1 true true 62 225 90 285
Rectangle -1 true true 30 75 210 225
Circle -1 true true 135 75 150
Circle -7500403 true false 180 76 116

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Rectangle -7500403 true true 195 106 285 150
Rectangle -7500403 true true 195 90 255 105
Polygon -7500403 true true 240 90 217 44 196 90
Polygon -16777216 true false 234 89 218 59 203 89
Rectangle -1 true false 240 93 252 105
Rectangle -16777216 true false 242 96 249 104
Rectangle -16777216 true false 241 125 285 139
Polygon -1 true false 285 125 277 138 269 125
Polygon -1 true false 269 140 262 125 256 140
Rectangle -7500403 true true 45 120 195 195
Rectangle -7500403 true true 45 114 185 120
Rectangle -7500403 true true 165 195 180 270
Rectangle -7500403 true true 60 195 75 270
Polygon -7500403 true true 45 105 15 30 15 75 45 150 60 120

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.3.1
@#$#@#$#@
setup
set grass? true
repeat 75 [ go ]
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
