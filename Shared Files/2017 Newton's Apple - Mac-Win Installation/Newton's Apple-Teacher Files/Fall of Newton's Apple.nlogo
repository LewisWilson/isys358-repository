breed [Central]
breed [Moon]
breed [satellites satellite]
breed [crashes crash]

turtles-own
[ Fx     ;; x-component of force vector
  Fy     ;; y-component of force vector
  Fz     ;; z-component of force vector
  vx     ;; x-component of velocity vector
  vy     ;; y-component of velocity vector
  vz     ;; z-component of velocity vector
  Ek     ;; kinetic energy - applies only to satellites
  Ep     ;; potential energy - applies only to satellites
  Jx     ;; x-compt angular momentum
  Jy     ;; y-compt angular momentum
  Jz     ;; z-compt angular momentum
  rx     ;; real x-coordinate (for when particle leaves world)
  ry     ;; real y-coordinate (for when particle leaves world)
  rz     ;; real z-coordinate (for when particle leaves world)
  m      ;; mass of turtle
  radius ;; radius of turtle - to allow for collisions
  rtheta  ;; angle of turtle wrt Central body - used only for setup NB angles are CW from y-direction
  vtheta  ;; curent angle of satellites velocity vector wrt to turtle at focus-centre
  name   ;; name of turtle
]
crashes-own [ countdown ]

globals
[
; turtle variables
  my-rx  ;; x-coordinate of the current turtle
  my-ry  ;; y-coordinate of the current turtle
  my-rz  ;; z-coordinate of the current turtle
  my-m   ;; mass of the current turtle

  temp-Fx ; variable that accumulates the x-component of the force on current turtle due to all the others
  temp-Fy ; variable that accumulates the y-component of the force on current turtle due to all the others
  temp-Fz ; variable that accumulates the z-component of the force on current turtle due to all the others
  temp-Ep ; variable that accumulates the potential energy of the current turtle due to all the others
 ;
 ; model parameters
  initial-radius
  central-size
  central-radius
  central-mass
  moon-radius
  moon-mass
  Earth-Moon-distance
;  sat-m-factor
;  G     ;; Gravitational Constant
  system-factor ; system specific scaling factor
  potential-energy-factor  ; set to 1/2 for cluster model but to 1 for central systems in which energy of central gravitiating body not included
;
; display parameters
;
; Location and velocity of new centre
  rx-centre
  ry-centre
  rz-centre
  vx-centre
  vy-centre
  vz-centre
;
  scale-factor ; factor by which all distances between objects are scaled.
  old-scale
  old-focus
;
; Earth-moon satellite velocity
  v
  v-rad
  v-orb
  delta-v
]
;;;;;;;;;;;;;;;;;;;;;;
;;; Demo Scripts   ;;;
;;;;;;;;;;;;;;;;;;;;;;
;
to run-demo
  reset-timer
    run-demo1
    run-demo4
    run-demo2
    run-demo2b
    run-demo3
    run-demo5
    run-demo6
end
;
;Challenge 1
to run-demo1                    ; shows how an object can be put in orbit --> Newton's canon
set system "Earth Orbit"
set-defaults
set fade-rate 0
while [ v-orbital < 4 ]
[
setup
repeat 60 +  12 * v-orbital ^ 2
[
go
]
set v-orbital v-orbital + 1
]
end
;
to run-demo4                    ; shows that an object in circular orbit keeps both KE & PE constant
set system "Earth Orbit"
set-defaults
set fade-rate 0
set v-orbital 4.327
setup
repeat 200 [ go ]
end
;
; Challenge 2
to run-demo2                     ; shows that different masses fall at the same rate
set system "Earth Orbit"
set-defaults
set number 10
set v-orbital 0
set mass 200
set random-mass? true
setup
repeat 60
[
go
]
end
;
to run-demo2b                     ; shows that different masses all have the same orbit
set system "Earth Orbit"
set-defaults
set number 10
set v-orbital 3.0
set mass 10
set fade-rate 0.1
set random-mass? true
setup
repeat 250
[
go
]
end
;
; Challenge 3
to run-demo3
set system "Earth Orbit"         ; when the masses are increased, they start to interact with each other
set-defaults
set number 10
set v-orbital 3.0
set mass 200
set fade-rate 0.1
set random-mass? true
setup
repeat 350 [ go ]
end
;
; Challenge 8
to run-demo5
 set system "Solar System"      ; how does orbital velocity depend on distance from the centre e.e the Solar System
 set-defaults
 set scale-index 1.4
 setup
 repeat 200 [ go ]
end
;
to run-demo6
  set system "Stellar Cluster"
  set-defaults
  setup
  repeat 150 [ go ]
  set scale-index 1.5
  repeat 100 [ go ]
end
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Set default slider values ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to set-defaults ; set default values for sliders, depending on system choice
    set magnification 1
    set delta-v 0
;
; set plot-zero
    set rx-centre 0
    set ry-centre 0
    set rz-centre 0

; set satellite velocities to zero
    set v-orb 0
    set v-rad 0
;
  if system = "Earth Orbit"
  [
    set G 6.67
    set scale-index 0
    set number 1
    set v-radial 0
    set v-orbital 0
    set set-Vorb? false
    set random-v? false
    set mass 20
    set random-mass? false
    set initial-altitude 15
    set initial-angle 0
    set random-radius? false
    set r-limit 10
    set fade-rate 0.5
    set focus "Earth"
    set delta-v 1
    set sat-m-factor 200
  ]
  if system = "Earth-Moon"
  [
    set G 6.67
    set scale-index 1.15
    set magnification 4
    set number 1
    set v-radial 0
    set v-orbital 0
    set set-Vorb? true
    set random-v? false
    set mass .1
    set random-mass? false
    set initial-altitude 40
    set initial-angle 45 + 180
    set random-radius? false
    set r-limit 10
    set fade-rate 0
    set focus "Earth"
    set delta-v 1
    set sat-m-factor 200
  ]
  if system = "Moon Orbit"
  [
    set G 6.67
    set scale-index -0.1
    set magnification 1
    set number 1
    set v-radial 0
    set v-orbital 0.6
    set set-Vorb? false
    set random-v? false
    set mass .1
    set random-mass? false
    set initial-altitude 360
    set initial-angle 45
    set random-radius? false
    set r-limit 10
    set fade-rate 0
    set Focus "Moon"
    set delta-v 0.1
    set sat-m-factor 100
  ]
  if system = "Solar System"
  [
    set G 6.67
    set scale-index 1.8
    set magnification 1.5
    set number 8
    set v-radial 0
    set v-orbital 0
    set set-Vorb? true
    set random-v? false
    set mass 4
    set random-mass? true
    set initial-altitude 30
    set random-radius? true
    set r-limit 50
    set fade-rate 0.2
    set sat-m-factor 1
  ]
     if system = "Stellar Cluster"
  [
    set G 300
    set scale-index 1.0
    set number 50
    set v-radial 0
    set v-orbital 5
    set set-Vorb? false
    set random-v? true
    set mass 400
    set random-mass? true
    set initial-altitude 50
    set random-radius? true
    set r-limit 100
    set fade-rate 4.5
    set sat-m-factor 1
  ]
   if system = "Galactic"
  [
    set G 20
    set scale-index 2.0
    set number 100
    set v-radial 0
    set v-orbital 0
    set set-Vorb? true
    set random-v? false
    set mass 20
    set random-mass? true
    set initial-altitude 50
    set random-radius? true
    set r-limit 50
    set fade-rate 5
    set sat-m-factor 100
  ]
end
;;;;;;;;;;;;;;;;;;;;;;;;
;;; Setup Procedures ;;;
;;;;;;;;;;;;;;;;;;;;;;;;
to setup
  clear-all
  set scale-factor 10 ^ scale-index ; scale factor is calculated from the index set on the interface slider
  set old-scale scale-factor
  set old-focus Focus
;
; the setup procedures are system dependent
  if system = "Earth Orbit"
  [
;
; Earth orbit settings
    set system-factor 10
    set central-radius 63.7
    set central-mass 600
    set potential-energy-factor 1
    set-default-shape Central "Earth"
    set-default-shape satellites "circle"
    set-default-shape crashes "circle"

    setup-Central
    setup-satellites
  ]
  if system = "Earth-Moon" or system = "Moon Orbit"
  [
;
; Earth-Moon settings
    set system-factor 10
    set central-radius 63.7
    set central-mass 600
    set moon-mass 7.35
    set moon-radius 17.4
    set Earth-Moon-distance 384
    set potential-energy-factor 1
    set-default-shape Central "Earth"
    set-default-shape Moon "circle"
    set-default-shape satellites "rocket"
    set-default-shape crashes "circle"
    set number 1

    setup-Central
    setup-moon
    setup-satellites
    reset-focus
  ]
  if system = "Solar System"
  [
;
; Solar System Settings
    set system-factor 500
    set central-radius 25
    set central-mass 333333.3
    set potential-energy-factor 1
    set-default-shape Central "circle"
    set-default-shape satellites "circle"

    setup-Central
    setup-planets
  ]
  if system = "Stellar Cluster"
  [
    set system-factor 50
    set central-radius 0
    set central-mass 0
    set potential-energy-factor 0.5
    set-default-shape satellites "circle"

    setup-satellites
    centre-system
  ]
  if system = "Galactic"
  [
    set system-factor 500
    set central-radius 1
    set central-mass 1e6
    set potential-energy-factor 1
    set-default-shape Central "Black Hole"
    set-default-shape satellites "circle"

    setup-Central
    setup-satellites

  ]

  set-KE-and-J

  display-system

  reset-ticks
end
;
; set up central or Earth
to setup-Central
  create-Central 1
   [
     set color yellow
     set m Central-mass
     ifelse system = "Earth Orbit" or system = "Earth-Moon"
     [ set size Central-radius * 2 / (scale-factor) * magnification ]
     [ set size Central-radius * 2 / (scale-factor) ^ ( 1 / 3 ) ]
     if system = "Galactic" [ set size 20 ]
     setxy 0 0
     set vx 0
     set vy 0
     set vz 0
     set radius Central-radius
   ]
end
;
; setup Moon
to setup-Moon
  create-Moon 1
  [
    set color grey + 3
    set m moon-mass
    set size moon-radius * 2 / (scale-factor) * magnification
    set rx Earth-Moon-distance * sin 45 * system-factor
    set ry Earth-Moon-distance * cos 45 * system-factor
    let r sqrt ( rx * rx + ry * ry )
    let v-moon sqrt ( G * central-mass / r )
;    show v-moon
    set vx -1 * v-moon * cos 45
    set vy v-moon * sin 45
    set vz 0
    set radius Moon-radius
    create-link-with turtle 0

  ]
end
;
;  Set up satellites
to setup-satellites

  let nTurtle 0
  let dAngle 360 / number
  let myAngle initial-angle
  while  [ nTurtle < number  ]
  [
    create-satellites 1
    [
      set color white
      set rtheta myAngle

; set mass of each satellite & corresponding size

      ifelse random-mass? [set m random-float mass ]  [ set m mass ]
      if system = "Earth Orbit" [ set m m / sat-m-factor ]
      resize-satellites

; set radial position of each satellite
      let r central-radius + initial-altitude * system-factor
      if random-radius? [ set r  central-radius * 1.5  + random-float (initial-altitude * system-factor) ]
      if r < central-radius
      [
        set r central-radius + 0.8
;        set initial-radius 6.37
      ]
      set rx r * sin rtheta
      set ry r * cos rtheta
      set rz r * 0

;
; set the initial orbital-velocity to sqrt(GM/r)
      if set-Vorb?
      [
        set v-orbital precision sqrt ( G * central-mass / r ) 3
        set v-radial 0
        set random-v? false
       ]
      ifelse random-v?
      [
        set vx random-float v-radial * sin rtheta - random-float v-orbital * cos rtheta
        set vy random-float v-radial * cos rtheta + random-float v-orbital * sin rtheta
        set vz 0
        set set-Vorb? false
      ]
      [
        set vx v-radial * sin rtheta - v-orbital * cos rtheta
        set vy v-radial * cos rtheta + v-orbital * sin rtheta
        set vz 0
      ]

      if system = "Earth-Moon" or system = "Moon Orbit"
      [
        set size 25
        set color red
        set heading atan ( vx - vx-centre ) ( vy - vy-centre )
      ]

    ]
    set myAngle myAngle + dAngle
    set nTurtle nTurtle + 1
  ]
    ask turtles with [breed != crashes and breed != Central]
  [
    update-force
    ifelse show-force?
    [
      set label precision ( sqrt ( Fx * Fx + Fy * Fy ) * 2000 / system-factor ) 3
      set label-color yellow
    ]
    [
      set label ""
    ]
  ]
end
;
;Setup planets for solar system model
to setup-planets
  let nTurtle 0
  set number 8
  while  [ nTurtle < number  ]
  [
    create-satellites 1
    [
;
; set radial position of each satellite
      let n nTurtle + 1
      set rtheta random 360
      let r (1.6302 - 2.4974 * n + 2.4867 * n ^ 2 - 0.881 * n ^ 3 + 0.1399 * n ^ 4 - 0.007 * n ^ 5 ) * system-factor
      set rx r * sin rtheta
      set ry r * cos rtheta
      set rz r * 0
;
; set the initial orbital-velocity to sqrt(GM/r)
      set v-orbital precision sqrt ( G * central-mass / r ) 3
      set v-radial 0

      set vx v-radial * sin rtheta - v-orbital * cos rtheta
      set vy v-radial * cos rtheta + v-orbital * sin rtheta
      set vz 0
;
; set mass of each satellite & corresponding size
      ifelse n = 1
      [
        set m 0.1
        set name "My  "
        set color white
      ]
      [
        ifelse n = 2
        [
          set m 0.8
          set name "V  "
          set color white
        ]
        [ ifelse n = 3
          [
            set m 1
            set name "E  "
            set color blue
          ]
          [
            ifelse n = 4
            [
              set m 0.1
              set name "M  "
              set color red
            ]
            [
              ifelse n = 5
              [
                set m 318
                set name "J  "
                set color orange
              ]
              [
                ifelse n = 6
                [
                  set m 95
                  set name "S  "
                  set color white
                ]
                [
                  ifelse n = 7
                  [
                    set m 14.5
                    set name "U  "
                    set color cyan
                  ]
                  [
                    set m 17.1
                    set name "N  "
                    set color cyan
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
      set size  (m / scale-factor ) ^ ( 1 / 3 ) * 4 * magnification
      if size < 3 [set size 3]


      set label name
      set label-color white
    ]

    set nTurtle nTurtle + 1
  ]
end


; remove translation of centre of mass for non-central model
to centre-system
;
; centre system on the centre of mass
;  let mean-x mean [ m /

; remove the motion of the centre of mass
  let mean-px mean [ vx * m ] of satellites
  let mean-py mean [ vy * m ] of satellites
  let mean-pz mean [ vz * m ] of satellites
  ask satellites
    [
      set vx vx - mean-px / m
      set vy vy - mean-py / m
      set vz vz - mean-pz / m
    ]

end
;
; set initial kinetic energy and angular momentum
to set-KE-and-J
  ask satellites
  [
; set kinetic energy
      set Ek 0.5 * m * ( vx * vx + vy * vy + vz * vz )
; set initial angular momentum
      set Jx 0
      set Jy 0
      set Jz ( rx * vy - ry * vx ) * m
  ]
;  show sum [Ek] of satellites
end

;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Runtime Procedures ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;
to go
;
; set the new scale-factor based on the current slider setting
  set scale-factor 10 ^ scale-index
; ..............and resize the objects in the model
  if system != "Galactic" [resize-Central]
  ifelse system = "Earth-Moon" or system = "Moon Orbit"
  [
    resize-Moon
    ask satellites
    [
      set heading atan ( vx - vx-centre ) ( vy - vy-centre )
    ]
  ]
  [resize-satellites]
;
;  For near-Earth scenarios satellites can crash & explode
  if system = "Earth Orbit" or system = "Earth-Moon" or system = "Moon Orbit"
  [
    check-for-crash
    explode-crashes
  ]
;
; All objects (turtles except crashes) gravitate with each of the others
  gravitate


  if system = "Earth-Moon" or system = "Earth Orbit" or system = "Moon Orbit"
  [
      reset-focus
  ]
;
; for stellar cluster, keep centre of mass of objects in the centre of the screen.
  if  system = "Stellar Cluster" [ centre-system ]
;
  display-system
;
; if the focus or the scale changes ckear all trails
  ifelse focus != old-focus or scale-factor != old-scale
  [
    clear-trails
  ]
  [
  fade-patches  ; Uri's code
  ]

    tick
end
;
; Spacecraft manoeuvre procedures
;
to boost
  if system = "Earth Orbit" or system = "Earth-Moon" or system = "Moon Orbit"
  [
    ask satellites
    [
      set vtheta atan (vx - vx-centre) (vy - vy-centre)
      let dvx delta-v * sin vtheta * sqrt ( G / 6.67 )
      let dvy delta-v * cos vtheta * sqrt ( G / 6.67 )
      set vx vx + dvx
      set vy vy + dvy
    ]
  ]
end
;
to slow
    if system = "Earth Orbit" or system = "Earth-Moon" or system = "Moon Orbit"
  [
    ask satellites
    [
      set vtheta atan (vx - vx-centre) (vy - vy-centre)
      let dvx delta-v * sin vtheta * sqrt ( G / 6.67 )
      let dvy delta-v * cos vtheta * sqrt ( G / 6.67 )
      set vx vx - dvx
      set vy vy - dvy
    ]
  ]
end
;
to nudge-left
    if system = "Earth Orbit" or system = "Earth-Moon" or system = "Moon Orbit"
  [
    ask satellites
    [
      set vtheta atan (vx - vx-centre) (vy - vy-centre)
      let dvx delta-v * sin ( vtheta + 90 ) * sqrt ( G / 6.67 )
      let dvy delta-v * cos ( vtheta + 90 ) * sqrt ( G / 6.67 )
      set vx vx - dvx
      set vy vy - dvy
    ]
  ]
end
;
to nudge-right
      if system = "Earth Orbit" or system = "Earth-Moon" or system = "Moon Orbit"
  [
    ask satellites
    [
      set vtheta atan (vx - vx-centre) (vy - vy-centre)
      let dvx delta-v * sin ( vtheta - 90 ) * sqrt ( G / 6.67 )
      let dvy delta-v * cos ( vtheta - 90 ) * sqrt ( G / 6.67 )
      set vx vx - dvx
      set vy vy - dvy
    ]
  ]
end


to resize-Central
  ask Central
  [
    ifelse system = "Earth Orbit" or system = "Earth-Moon" or system = "Moon Orbit"
     [ set size Central-radius * 2 / (scale-factor)  * magnification ]
     [ set size Central-radius * 2 / (scale-factor) ^ ( 1 / 3 ) ]

  ]
end

to resize-Moon
  ask Moon
  [
    set size ( Moon-radius * 2 / scale-factor )  * magnification
  ]
end
to resize-satellites
  ask satellites
  [
    set size  (m * sat-m-factor / scale-factor ) ^ ( 1 / 3 ) * 4 * magnification
    if size < 4 [set size 4 ]
  ]
end

to gravitate ;; Turtle Procedure

  ask turtles with [breed != crashes and breed != Central]
  [
    update-force
    ifelse show-force?
    [
      set label precision ( sqrt ( Fx * Fx + Fy * Fy ) * 2000 / system-factor ) 3
      set label-color yellow
    ]
    [
      set label ""
    ]
  ]
  ask turtles with [breed != crashes and breed != Central]
  [
    update-velocity
    update-position
  ]
;
;  ask moon [ update-force ]
;  ask moon
 ; [
;  update-velocity
;  update-position
;  ]

end
;
; check for crash on Earth
to check-for-crash
  ask satellites
  [
    set my-rx rx
    set my-ry ry
    set my-rz  0
    let crashed false

    ask other turtles
    [
      let delta-x rx - my-rx
      let delta-y ry - my-ry
      let delta-z rz - my-rz

      let r-sqrd delta-x * delta-x + delta-y * delta-y + delta-z * delta-z
      if r-sqrd <= radius * radius [ set crashed true ]
    ]
    if crashed
    [
      set breed crashes
      set vx vx-centre
      set vy vy-centre

      set m 0
      set color red
      set size 30 / scale-factor
      set countdown 1
    ]

  ]

end

to explode-crashes
   ask crashes
  [
    set countdown countdown + 1
    set size size + 1
    set rx rx + vx
    set ry ry + vy
    if countdown > 20
    [
    die
    ]
    ]
end
;

to update-force ;; Turtle Procedure
  set temp-Fx 0
  set temp-Fy 0
  set temp-Fz 0
  set temp-Ep 0
  set my-rx rx
  set my-ry ry
  set my-rz  0
  set my-m m
  ask other turtles    ; each of the other turtles, except "myself" , contributes to the force on this turtle
  [

      let delta-x rx - my-rx
      let delta-y ry - my-ry
      let delta-z rz - my-rz

      let r-sqrd delta-x * delta-x + delta-y * delta-y + delta-z * delta-z
      if r-sqrd < r-limit * r-limit [ set r-sqrd r-limit * r-limit ]
      let r sqrt  r-sqrd

      ;; Calculate component forces on "myself" due to this mass using inverse square law
      let F G * my-m * m / r-sqrd

      let delta-fx F * (delta-x / r)
      let delta-fy F * (delta-y / r)
      let delta-fz F * (delta-z / r)

      set temp-Fx temp-Fx + delta-fx
      set temp-Fy temp-Fy + delta-fy
      set temp-Fz temp-Fz + delta-fz

      set temp-Ep temp-Ep - potential-energy-factor * G * my-m * m / r
  ]
  set Fx temp-Fx
  set Fy temp-Fy
  set Fz temp-Fz
;  if breed = "satellites" [
    set Ep temp-Ep
;    ]

end

to update-velocity ; and Kinetic energy ;; Turtle Procedure
  ;; Now we update each particle's velocity, by taking the old velocity and
  ;; adding the force to it.

  let dvx Fx / m
  let dvy Fy / m
  let dvz Fz / m
  set vx (vx + dvx)
  set vy (vy + dvy)
  set vz (vz + dvz)
;  if breed = "satellites"
;  [
    set Ek 0.5 * m * ( vx * vx + vy * vy + vz * vz )
    set Jz ( rx * vy - ry * vx ) * m
;  ]


end

to update-position ;; Turtle Procedure
  set rx (rx + vx)
  set ry (ry + vy)
  set rz (rz + vz)
  set rtheta atan rx ry
;  show theta

end
;
to reset-focus
   if Focus = "Moon"
   [
     ask moon
     [
       set rx-centre rx
       set ry-centre ry
       set vx-centre vx
       set vy-centre vy
     ]
     if focus != old-focus
     [
     set scale-index -0.1
     set magnification 1
     ]
   ]
   if Focus = "Earth"
   [
     ask Central
     [
       set rx-centre rx
       set ry-centre ry
       set vx-centre vx
       set vy-centre vy
     ]
     if focus != old-focus
   [
     set scale-index 1.15
     set magnification 4
   ]
   ]
  ; transform all turtle positions - velocities remain unchanged & transformation achieved by updating location each iteration
  ask turtles
  [
    set rx rx - rx-centre
    set ry ry - ry-centre
  ]


end
;
; last step of each iteration is to display all the objects in the visible space
to display-system
ask turtles
[
  let rx-plot rx
  let ry-plot ry
  ifelse ( abs rx-plot / scale-factor < max-pxcor ) and ( abs ry-plot / scale-factor < max-pycor )
  [
    show-turtle
    setxy (rx-plot / scale-factor) (ry-plot / scale-factor)
    if (fade-rate != 100)   [ set pcolor yellow ]
  ]
; turtle is outside of display space => park turtle on circle of radius max-pxcor and at the correct angle to the centre.
  [
    let r sqrt ( rx * rx + ry * ry )
    let r-plot 0.99 * max-pxcor
    set rx-plot r-plot * rx-plot / r
    set ry-plot r-plot * ry-plot / r

   setxy rx-plot ry-plot
   hide-turtle
  ]
]
end
;
;
to clear-trails
  ask patches with [pcolor != black]
  [
    set pcolor black
  ]
  set old-focus focus
  set old-scale scale-factor
end

to fade-patches ;  --- this and the following procedures are copyright Uri Wilensky 1988
  ask patches with [pcolor != black]
  [ ifelse (fade-rate = 100)
    [ set pcolor black ]
    [ if (fade-rate != 0)
      [ fade ]
    ]
  ]
end

to fade ;; Patch Procedure copyright Uri Wilensky 1988
  let new-color pcolor - 8 * fade-rate / 100
  ;; if the new-color is no longer the same shade then it's faded to black.
  ifelse (shade-of? pcolor new-color)
  [ set pcolor new-color ]
  [ set pcolor black ]
end


; Copyright 2015 Paul Stokes
; See Info tab for full copyright and license.
@#$#@#$#@
GRAPHICS-WINDOW
576
10
1187
642
300
300
1.0
1
15
1
1
1
0
0
0
1
-300
300
-300
300
1
1
1
ticks
10.0

BUTTON
337
65
430
98
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
126
491
267
524
number
number
1
100
10
1
1
particles
HORIZONTAL

SLIDER
439
108
570
141
mass
mass
.1
400
200
0.1
1
NIL
HORIZONTAL

BUTTON
455
65
548
101
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
422
207
544
240
fade-rate
fade-rate
0
5
0
.2
1
%
HORIZONTAL

SLIDER
287
107
433
140
v-orbital
v-orbital
0
100
3
0.1
1
km/s
HORIZONTAL

SLIDER
129
577
221
610
r-limit
r-limit
0
100
10
1
1
NIL
HORIZONTAL

SLIDER
33
206
252
239
scale-index
scale-index
-0.5
3
0
.1
1
NIL
HORIZONTAL

SLIDER
287
144
433
177
v-radial
v-radial
0
100
0
.1
1
NIL
HORIZONTAL

PLOT
68
251
558
480
System Energy
NIL
NIL
0.0
1.0
-0.1
0.1
true
true
"" ""
PENS
"Kinetic Energy" 1.0 0 -15040220 true "" "plot sum [ Ek ] of satellites"
"Pot Energy" 1.0 0 -2674135 true "" "plot sum [ Ep ] of satellites"
"Total Energy" 1.0 0 -13345367 true "" "plot sum [ Ek + Ep ] of satellites"

SWITCH
126
529
267
562
random-radius?
random-radius?
1
1
-1000

SWITCH
274
492
414
525
random-mass?
random-mass?
0
1
-1000

CHOOSER
8
66
128
111
System
System
"Earth Orbit" "Solar System" "Stellar Cluster" "Galactic"
0

SWITCH
438
579
547
612
set-Vorb?
set-Vorb?
1
1
-1000

BUTTON
138
66
231
99
NIL
set-defaults
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
274
528
414
561
random-v?
random-v?
1
1
-1000

SLIDER
136
106
283
139
initial-altitude
initial-altitude
0
400
15
.1
1
Mm
HORIZONTAL

SLIDER
137
143
281
176
initial-angle
initial-angle
0
360
0
5
1
NIL
HORIZONTAL

SLIDER
307
206
418
239
magnification
magnification
1
10
1
1
1
NIL
HORIZONTAL

CHOOSER
22
572
114
617
Focus
Focus
"Earth"
0

TEXTBOX
9
45
159
63
Setup Scenario
13
0.0
1

TEXTBOX
32
186
151
204
Display Controls\n
13
0.0
1

TEXTBOX
25
498
175
540
Multi-Satellite\nOptions
13
0.0
1

SLIDER
231
578
323
611
G
G
0
100
6.67
1
1
NIL
HORIZONTAL

TEXTBOX
64
124
119
172
Satellite\nSetup
13
0.0
1

SLIDER
339
578
431
611
sat-m-factor
sat-m-factor
0
400
200
1
1
NIL
HORIZONTAL

TEXTBOX
8
565
559
622
NIL
11
0.0
0

SWITCH
440
144
571
177
show-force?
show-force?
1
1
-1000

TEXTBOX
9
10
349
35
The Fall of Newton's Apple
20
0.0
1

@#$#@#$#@
# Version 2.4
## WHAT IS IT?

This is a model of Newtonian Universal Gravitation. It shows how the same law descibes behaviour on four very different distance and mass scales:
 - Earth orbit
 - Earth-Moon system
 - the Solar System
 - a modest sized cluster of stars
 - a large number of stars orbiting a massive black hole at the centre of the galaxy

## HOW TO USE IT

Select the "Earth Orbit" system and press "set-defaults". An object will appear above the Earth's surface. Immediately below the "set-defaults" button are 2 sliders:

  *  The first one ("v-radial") allows you to set the object's initial velocity in the radial direction. This is the direction that is towards or away from the centre of the Earth;

  *  The second slider ("v-orbital") allows you to set the object's initial velocity in the orbital direction at 90 degrees to the radial.

See if you can get the object into orbit?

You can also set the objects "initial-altitude", in 1000s of kms above the surface of the Earth and at different angles.

The "Mass" slider sets the value of the mass of the object. The units are arbitrary; only relative values are important.

You can increase the number of satellites and set both the initial radius and mass values to random values, up to the value set on the slider.

The "scale-index" allows you to zoom into and out of the model and is logarithmic. Scale-index of 0 corresponds to an actual scale factor of 1 and 1 corresponds to 10 accordingly.

The "magnification" slider allows you to increase the size of satellites without changing overall distance scales. This allows you ta make the satellites easier to see when the overall "scale-index" is increased.

The "fade-rate" slider allows you to control how rapidly the satellite trails fade away. If this set to zero you can produce some interesting patterns.

The "Multi-Satellite" options allow you set up a number of satellites at random altitudes and with random masses. The "V-orb?" switch allows you set the satellites to all have circular orbits.


## THINGS TO TRY

Try setting up a number of satellites with "randon-mass?" set on.

Look at the other systems and try different setting and numbers of objects. If things are moving too slowly, use the "speed" slider at the top of the screen.

The defaults for the "Solar System" shows the Planets at their appropriate scale distances. The sizes of the planets are not to scale as they would be invisible. Note also that the muli-satellite options do not operate for this system.

In the "Stellar Cluster" system, there is no central gravitating mass and the "stars" move under their mutual gravitation.

The "Galactic" system, includes a very massive central body that simulates the super-massive black hole at the centre of the galaxy.

## HOW IT WORKS

The model is based on Newton's Law of Universal Gravitation:

>F = Gm<sub>1</sub>m<sub>2</sub> / r<sup>2</sup>

Each object in the model experiences the vector sum of all of the forces due to each other body in the model.

The net force on each object determines its change in velocity according to Newton's 2nd Law:

> F = m * a

The model then updates each objects position based on its velocity.


## NETLOGO FEATURES

The "Infinite Plane was devised by Uri Wilensky (see below), although its implimentation here is a different from the original.

In addition, the feature allowing the paths of the objects to be observed as fading trails was also devised by Uri Wilensky and incorporated here, largely unchanged.


## CITATION

* Wilensky, U. (1998).  NetLogo Gravitation model.  http://ccl.northwestern.edu/netlogo/models/Gravitation.  Center for Connected Learning and Computer-Based Modeling, Northwestern Institute on Complex Systems, Northwestern University, Evanston, IL.
* Wilensky, U. (1999). NetLogo. http://ccl.northwestern.edu/netlogo/. Center for Connected Learning and Computer-Based Modeling, Northwestern Institute on Complex Systems, Northwestern University, Evanston, IL.

## COPYRIGHT

Copyright (c) 2017 Pallas Advanced Learning Systems Pty Ltd. All rights reserved.
This product may only be used with a valid license from Pallas Advanced Learning Systems Pty Ltd. Unauthorized use of this product is not allowed under Australian law.

Version v2.42
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

black hole
false
0
Rectangle -2674135 true false 148 0 155 296
Rectangle -2674135 true false 3 149 295 157

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

earth
false
0
Circle -13345367 true false 1 0 297
Polygon -6459832 true false 96 184 113 171 120 181 141 178 121 150 152 166 192 178 207 195 208 224 202 240 186 243 167 235 154 225 136 227 111 237 108 252 101 258 93 254 58 240 92 200
Polygon -10899396 true false 54 170 85 141 118 134 92 158
Polygon -13345367 true false 291 192 265 210 234 233 205 257 151 298 185 290 224 277 250 258 267 238 286 213 294 187
Polygon -1 true false 203 107 213 119 211 134 172 101 203 108
Polygon -1 true false 139 54 149 66 147 81 136 73 139 55
Polygon -1 true false 217 20 227 32 225 47 214 39 217 21
Polygon -1 true false 300 210 150 300 181 292 218 283 248 263 271 240 281 222
Polygon -14835848 true false 34 57 18 103 18 128 10 157 22 179 14 200 34 214 13 206 3 184 2 150 5 112 15 83

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

rocket
true
0
Polygon -7500403 true true 120 165 75 285 135 255 165 255 225 285 180 165
Polygon -1 true false 135 285 105 135 105 105 120 45 135 15 150 0 165 15 180 45 195 105 195 135 165 285
Rectangle -7500403 true true 147 176 153 288
Polygon -7500403 true true 120 45 180 45 165 15 150 0 135 15
Line -7500403 true 105 105 135 120
Line -7500403 true 135 120 165 120
Line -7500403 true 165 120 195 105
Line -7500403 true 105 135 135 150
Line -7500403 true 135 150 165 150
Line -7500403 true 165 150 195 135

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -2674135 true false 270 75 225 30 30 225 75 270
Polygon -2674135 true false 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.3.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
