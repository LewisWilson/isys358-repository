﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneSpawner : MonoBehaviour {

	//Prefabs
	public Satellite satPrefab;
	public Center earthPrefab;


	//GameObjects
	private Center earth;

	//Variables from sliders
	private float initialAltitude;
	private float initialAngle;
	private float mass;
	private float magnification;
	private int numSatellites;
	private bool randomRadius;
	private bool randomMass;
	private bool vRandom;


	//Global Variables
	private float rotationSpeed = 0.1f;
	private Rigidbody rb;
	private Vector3 myTransform;
	private bool Motion;
	private int destroyedSatNum;
	private float gravity;

	//Sliders
	//Setup Scenario
	public Button setup;
	public Button go;
	public Button setDefault;

	//Setup Satellites
	public Slider initAltSlide;
	public Slider initAngleSlide;
	public Slider vOrbSlide;
	public Slider vRadSlide;
	public Slider massSlide;
	public Toggle showForce;

	//Display Controls
	public Slider scaleIndex;
	public Slider magSlide;
	public Slider fadeRate;

	//Multi-Satellite Options
	public Slider satNumSlide;
	public Toggle randMass;
	public Toggle randRad;
	public Toggle randVel;

	void Start () {
		clickSetDefault ();

		rb = GetComponent<Rigidbody> ();

		UnityEngine.Events.UnityAction clickSet = () => { this.clickSetup(); };
		setup.onClick.AddListener(clickSet);

		UnityEngine.Events.UnityAction clickDefault = () => { this.clickSetDefault(); };
		setDefault.onClick.AddListener(clickDefault);

		UnityEngine.Events.UnityAction clickRun = () => { this.clickGo(); };
		go.onClick.AddListener(clickRun);

	}

	void FixedUpdate () {
		if (Motion == true) {
			play ();
		}
	}

	//Called when the setup Button is clicked
	void clickSetup(){
		Motion = false;
		destroyedSatNum = 0;

		//Values from the sliders, to make referencing them easier
		initialAltitude = initAltSlide.value;
		initialAngle = initAngleSlide.value;
		mass = massSlide.value;
		numSatellites = (int)satNumSlide.value;
		magnification = magSlide.value;
		randomMass = randMass.isOn;
		randomRadius = randRad.isOn;
		vRandom = randVel.isOn;
		float vOrbital = vOrbSlide.value;
		float vRadial = vRadSlide.value;
		float radius;

		// Creating the center object
		if (GameObject.Find (earthPrefab.name) == null) {
			earth = Instantiate (earthPrefab);
			earth.transform.parent = transform;
			earth.gameObject.name = "Earth";
		}

		Satellite[] satellites = FindObjectsOfType<Satellite> ();
		for (int i = 0; i < satellites.Length; i++) {
			satellites [i].DestroyGameObject ();
		}


		//Instantiating all of the satellites
		for (int i = 0; i < numSatellites; i++) {
			Satellite satellite = Instantiate (satPrefab);

			// Setting the mass value
			if (randomMass == true) {
				satellite.mass = Random.Range (0.1f, 400.0f);
			} else {
				satellite.mass = mass;
			}

			// Setting the value for radius
			float power = 1f / 3f;
			if (randomRadius == true) {
				float temp = Random.Range (0.1f, 400.0f);
				radius = (Mathf.Pow(temp, power)) * magnification;
			} else {
				radius = (Mathf.Pow(satellite.mass, power)) * magnification;
			}
			satellite.radius = radius;

			// Moving the satellite under the satellite spawner object in the heirachy
			satellite.transform.parent = transform;
			satellite.gameObject.name = "Satellite_" + (i + 1);

			// Calculating the position of each satellite in the circle (in degrees), then converting it to radians
			float separation = 360.0f / (float) numSatellites;
			float value = (initialAngle + (separation * i)) * Mathf.Deg2Rad;
			satellite.rTheta = value;

			//float tempSystemFactor = 10.0f;
			//replacing n with i for now however doesnt line up with code on line 
			//set mass to each satellite & corresponding size r r r r r///r se
			//float r = (1.6302f - 2.4974f * i + 2.4867f * Mathf.Pow(i,2) - 0.881f * Mathf.Pow(i,3) + 0.1399f * Mathf.Pow(i,4) - 0.007f * Mathf.Pow(i,5) ) * tempSystemFactor;


			// Getting the x and y position of each satellite (relative to the planet)
			float x = initialAltitude * Mathf.Sin (value);
			float y = initialAltitude * Mathf.Cos (value);
			satellite.transform.position = new Vector3 (x, y, 0);

			// Setting the variables for each satellite, including mass, size and vOrbital
			satellite.center = FindObjectOfType<Center> ().transform;

			// Setting the mass of the satellite
			Rigidbody rb = satellite.GetComponent<Rigidbody> ();
			rb.mass = satellite.mass;

			//Getting the gravitaional force of the planet on the satellite
			Rigidbody earthRigidbody = earth.GetComponent<Rigidbody> ();

			//Distance of the satellite from the planet
			float dist = Mathf.Sqrt ((Mathf.Pow (x, 2) + Mathf.Pow (y, 2)));

			//getting the gravitational pull
			//assume r is the distnce 
			float vOrb = Mathf.Pow(gravity * (earthRigidbody.mass / dist), power);
			float rTheta = satellite.rTheta;

			//Getting the x and y vector velocity
			float xVel = vRadial * Mathf.Sin(rTheta) - vOrb * Mathf.Cos(rTheta);
			float yVel = vRadial * Mathf.Cos (rTheta) + vOrb * Mathf.Sin (rTheta);

			//Setting the initial velocity
			satellite.angMomentum = new Vector3 (xVel, yVel, 0);

			//Setting the initial Force
			satellite.force = new Vector3(0, 0, 0);

			// Changing the size of the satellite
			satellite.transform.localScale = (satellite.transform.localScale * radius);
		}

		// Changing the size of the planet
		earth.transform.localScale = new Vector3(8, 8, 8) * magnification;
	}

	void clickSetDefault (){
		if (earth != null)
			earth.transform.position = new Vector3 (0, 0, 0);

		//Setting global variables
		Motion = false;
		destroyedSatNum = 0;
		gravity = 6.67f;

		//Setup Satellites
		initAltSlide.value = 200.0f;
		initAngleSlide.value = 0;
		vOrbSlide.value = 0;
		vRadSlide.value = 0;
		massSlide.value = 200.0f;
		showForce.isOn = false;

		//Display Controls
		scaleIndex.value = 1.5f;
		magSlide.value = 2;
		fadeRate.value = 0.5f;

		//Multi-Satellite Options
		satNumSlide.value = 5;
		randMass.isOn = false;
		randRad.isOn = false;
		randVel.isOn = false;
	}

	//Called when the Go button is pressed
	//Essentially pausing and unpausing the motion whenever the go button is pressed
	void clickGo(){
		if (Motion == false) {
			Motion = true;
		} else {
			Motion = false;
		}
	}

	//This function is called in Update() if motion is true
	void play(){
		Satellite [] satellites = GetComponentsInChildren<Satellite> ();
		Rigidbody earthRigidbody = earth.GetComponent<Rigidbody> ();
		//Setting values of the sliders
		numSatellites = (int)satNumSlide.value;
		magnification = magSlide.value;

		float radius;
		int end = satellites.Length;

		for (int i = 0; i < end; i++) {


			//Getting the x and y position of each satellite (relative to the planet)
			float xPos = satellites[i].transform.position.x;
			float yPos = satellites[i].transform.position.y;


			//Getting the relative coordinates of the satellite to the planet
			float deltaX = xPos - earthRigidbody.transform.position.x;
			float deltaY = yPos - earthRigidbody.transform.position.y;

			//Getting the distance of the satellite from the planet, as rad and its squared form in radSqrd
			float radSqrd = Mathf.Pow (deltaX, 2) + Mathf.Pow (deltaY, 2);
			//Not sure what this if statement is for
			// if r-sqrd < r-limit * r-limit [ set r-sqrd r-limit * r-limit ]
			float rad = Mathf.Sqrt(radSqrd);
			//print ("rad - " + rad);
			//print ("radSqrd - " + radSqrd);

			//Getting the value for the current force on the satellite
			float force = gravity * earthRigidbody.mass * (satellites[i].mass / radSqrd);
			//print ("force - " + force);

			//Updating the x and y force values
			//Getting the previous value for the force
			float xForce = satellites[i].force.x;
			float yForce = satellites[i].force.y;
			//Getting the force on the current frame
			float deltaFX = force * (deltaX / rad);
			float deltaFy = force * (deltaY / rad);
			//Updating the x and y force values with the curent force value
			xForce += deltaFX;
			yForce += deltaFy;

			//Saving the force value for this satellite in its force vector
			satellites [i].force = new Vector3 (xForce, yForce, 0);

			//Delta veolcity???
			float deltaVX = xForce / satellites[i].mass;
			float deltaVY = yForce / satellites[i].mass;

			//Updating the x and y velocity
			float xVel = satellites[i].angMomentum.x;
			float yVel = satellites[i].angMomentum.y;
			xVel -= deltaVX;
			yVel -= deltaVY;
			satellites [i].angMomentum = new Vector3 (xVel, yVel, 0);

			//Updating the current position based on the previous position and current velocity
			xPos += xVel;
			yPos += yVel;


//NOTE!!!!!!!!!!!!!!!!!
// i packed these eqautions together and is doesnt wark yet
			//float deltaX = rx - myRx;
			//F = G * myM * m / rSqrd;
			// rememebr to crete a gravity veriable

			//deltaFx = F * (deltaX / r);
			//assume r is radias but it might not be
			//deltaFy = F * (deltaY / r);
			//float Fx += F * (deltaX / satellites[i].radius);
			//float Fy += F * (deltaY / satellites[i].radius);


			//Setting the new position
			satellites [i].transform.position = new Vector3 (xPos, yPos, 0);


			//Test for if the satellite collides with the planet
			//Getting Distance of each satellite from the center of the planet
			float xDist = satellites[i].center.transform.position.x - xPos;
			float yDist = satellites[i].center.transform.position.y - yPos;

			//Getting the radius of the satellites
			float xRad = satellites[i].center.localScale.x;
			float yRad = satellites[i].center.localScale.y;

			//The actual test
			if ((xDist < xRad && xDist > -xRad) && (yDist < yRad && yDist > -yRad)) {
				satellites [i].DestroyGameObject ();
				destroyedSatNum += 1;
			}
		}

		// Changing the size of the planet
		earth.transform.localScale = new Vector3(8, 8, 8) * magnification;
	}
}

//  vx = v-radial * sin rtheta - v-orbital * cos rtheta
//  vy = v-radial * cos rtheta + v-orbital * sin rtheta

