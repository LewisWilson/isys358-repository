﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph : MonoBehaviour {
	private float graphWidth;
	private float graphHeight;
	LineRenderer newLineRenderer;
	List<int> force;
	int vertexAmount = 50;
	float xInterval;

	GameObject parentCanvas;
	// Use this for initialization
	void Start () {
		//find the game object and set the width and height to the size od the canvas
		parentCanvas = GameObject.Find ("Graph");
		graphWidth = transform.Find("Linerenderer").GetComponent<RectTransform>().rect.width;
		graphHeight = transform.Find("Linerenderer").GetComponent<RectTransform>().rect.height;
		//space the vertex amount evenly throught the width of the graph
		newLineRenderer.SetVertexCount(vertexAmount);
		xInterval = graphWidth / vertexAmount;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Draw(List<float> force){

		if (force.Count <= 0)
			return;
		float x = 0;

		for (int i = 0; i < vertexAmount && i < force.Count; i++) {
			int temp = force.Count - i - 1;
			float y = force [temp] * (graphHeight / 130);
			x = i * xInterval;

			newLineRenderer.SetPosition (i, new Vector3 (x - graphWidth / 2, y - graphHeight / 2, 0));

		}




	}

}
