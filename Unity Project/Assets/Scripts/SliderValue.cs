﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderValue : MonoBehaviour {

	public Slider slider;
	public Text sliderValue;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		sliderValue.text = slider.value.ToString ();
	}
}
