﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main_Camera : MonoBehaviour {

	public Slider scaleIndex;
	public Camera mainCam;

	public GameObject setScenario;
	public GameObject setSatellites;
	public GameObject dispControls;
	public GameObject multiSatOpt;
	public Button satMenu;
	public Button help;
	public Button quit;

	private int menuSlideSpeed;
	private bool minimised;

	private float startPos;
	private float minPos;

	// Use this for initialization
	void Start () {
		UnityEngine.Events.UnityAction clickSatMenu = () => { this.SatMenu(); };
		satMenu.onClick.AddListener(clickSatMenu);
		UnityEngine.Events.UnityAction clickHelp = () => { this.helpMenu(); };
		help.onClick.AddListener(clickHelp);
		UnityEngine.Events.UnityAction clickQuit = () => { this.exitApp(); };
		quit.onClick.AddListener(clickQuit);

		minimised = false;
		menuSlideSpeed = 0;
		startPos = setScenario.transform.position.x - 1;
		minPos = 0;
	}
	
	// Update is called once per frame
	void Update () {

		//-100 = -0.5
		// -3000 = 3
		//Calculating the zoom of the camera
		float x = this.transform.position.x;
		float y = this.transform.position.y;
		float z = -((scaleIndex.value + 0.5f) * 120.0f) - 20.0f;
		this.transform.position = new Vector3(x, y, z);

		//Minimising the menu
		Vector3 scenarioPos = setScenario.transform.position;
		Vector3 satPos = setSatellites.transform.position;
		Vector3 dispPos = dispControls.transform.position;
		Vector3 satOptPos = multiSatOpt.transform.position;

		scenarioPos.x += menuSlideSpeed;
		satPos.x += menuSlideSpeed;
		dispPos.x += menuSlideSpeed;
		satOptPos.x += menuSlideSpeed;
		minPos += menuSlideSpeed;

		setScenario.transform.position = scenarioPos;
		setSatellites.transform.position = satPos;
		dispControls.transform.position = dispPos;
		multiSatOpt.transform.position = satOptPos;

		if (minimised == true && minPos <= -5) {
			menuSlideSpeed = 0;
		} else if (minimised == false && setScenario.transform.position.x >= 0) {
			menuSlideSpeed = 0;
		}

		/*
		float indexVal = scaleIndex.value;
		mainCam.fieldOfView = (indexVal * 35) + 25;
		*/
	}

	void SatMenu() {
		if (minimised == false) {
			menuSlideSpeed = -1;
			minimised = true;
		} else {
			menuSlideSpeed = 1;
			minimised = false;
		}
	}

	void helpMenu(){
		Application.Quit ();
	}

	void exitApp(){
		Application.Quit ();
	}
}
