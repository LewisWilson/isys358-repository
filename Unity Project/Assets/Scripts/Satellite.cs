﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Satellite : MonoBehaviour {

	public Transform center;
	public float vOrbital;
	public float vRadial;
	public Vector3 force;
	public Vector3 angMomentum;
	public float mass;
	public float radius;
	public float rTheta;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
	}

	public void DestroyGameObject (){
		Destroy (gameObject);
	}
}
